# Heatpump Common Ontology

This project contains the Heatpump Common Ontology (HCO) and the documentations about the ontology.
The Heatpump Common Ontology is developed in the Dutch Team Duurzaam Installeren Consortium
to capture the data from a heatpump and its surrounding system. 

## Maintainers

Jack Verhoosel, TNO (jack.verhoosel@tno.nl) Klaas Andries de Graaf, TNO (klaas.degraaf@tno.nl) Jeanine de Graaf, TNO (jeanine.degraaf@tno.nl)


## Method

The HCO was developed with domain experts and focuses on formalizing the data that can be extracted from a heatpump. 

In collaboration with domain experts the Heatpump Common Dataset (HCD) was defined. Which shows al parameters that installers would like to get from a heatpump and its surroundings. 

![Alt text](HCD v4.0.PNG)

Based on this HCD, already existing standards were consulted. A lot of the parameters fit in [SAREF & extensions](https://saref.etsi.org/). So the HCO is build as an extension to SAREF and also reuses the Ontology of units of Measure [(OM)](http://www.ontology-of-units-of-measure.org/page/om-1.8.) 


## Visual
![Alt text](HCO-V1.0.PNG)

## Documentation
In the folder [Documentation](https://gitlab.com/tdi500/heatpump-common-ontology/-/tree/main/Documentation?ref_type=heads) you can find a copy of the visualizations above. 
You can also find  examples on how to instantiate the ontology with actual data. In the [slide deck](https://gitlab.com/tdi500/heatpump-common-ontology/-/blob/main/Documentation/HCO%20examples%20UC2.pptx?ref_type=heads) the relation between the HCD, HCO and rdf code is visualized step by step. 
In the picture below you can see an example of how the electricity meter accuracy of the heatpump, defined in the HCD, relates to the visuaization of the HCO and also to the rdf code that is used to instantiate the data of the heatpump.

![Alt text](HCD_HCO_example.PNG)

 


## Partners

Members of the [Team Duurzaam Installeren](https://teamduurzaaminstalleren.nl) Consortium:
Essent, 
Eneco, 
Feenstra, 
Breman, 
Kemkens, 
Bonarius, 
Mampaey, 
HeatTransformers, 
Mensen Maken de Transitie, 
Comfort Partners, 
TNO


